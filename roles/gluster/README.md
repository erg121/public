gluster
=========

Установка glusterfs сервера.

Requirements
------------

Хосты должны быть зарегистрированы на satellite.

используемые подписки:

- `ORGLOT_OFFICE_centos7_gluster38_centos7_gluster38_repo`

Role Variables
--------------
- `glusterfs_arbiter: 1/0` - наличие арбитратора в группе.
- `reinit: 1` - peer detach && volume delete для полной переустановки.
- `glusterfs_brick_path: /data/glusterfs/vol_shared/brick1`
- `glusterfs_volume_name: vol`
- `defaults/main.yml`

Dependencies
------------

Example inventory
-----------------
```text
[glfs]
board-glfs1.tsed.orglot.office ansible_host=10.200.81.17
board-glfs2.tsed.orglot.office ansible_host=10.200.81.18

[arbitrator]
board-adm.tsed.orglot.office ansible_host=10.200.81.11

[glusterfs:children]
glfs
arbitrator

```


Example Playbook
----------------
```yaml
- hosts: glusterfs
  become: yes

  roles:
    - role: gluster
      reinit: yes
      tags: glusterfs
```
License
-------

BSD

Author Information
------------------

evgeny.zemlyachenko@stoloto.ru
