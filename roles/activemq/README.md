activemq
=========

Установка activemq для шины данных.

Requirements
------------

Хосты должны быть зарегистрированы на satellite.

Role Variables
--------------

`reinstall: true - разрешает удаление activemq и пользователя`

`defaults/main.yml - параметры по умолчанию`

Dependencies
------------

```yaml
роли:
  - gluster-client
пакеты:
  - java-1.8.0-oracle (устанавливается автоматически. см. meta/main.yml)
```

Example inventory
-----------------
```text
[esb]
board-esb1.tsed.orglot.office ansible_host=10.200.81.15
board-esb2.tsed.orglot.office ansible_host=10.200.81.16
```


Example Playbook
----------------
```yaml
- hosts: esb
  become: yes

  roles:
    - role: activemq
      tags: activemq
```
License
-------

BSD

Author Information
------------------

evgeny.zemlyachenko@stoloto.ru
