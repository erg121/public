haproxy
=========

Установка haproxy балансировщика.

Requirements
------------


Role Variables
--------------
- `haproxy_groups` - список наименований групп хостов для однотипной балансировки (см. templates/haproxy.cfg.j2)
- `haproxy_template: haproxy.cfg.j2` - темплейт по умолчанию
- `haproxy_login: monitor` - логин для страницы статистики
- `haproxy_password: ***` - пароль для страницы статистики

Dependencies
------------

Example inventory
-----------------
```text
[proxy]
board-proxy1.tsed.orglot.office ansible_host=10.200.81.20
board-proxy2.tsed.orglot.office ansible_host=10.200.81.21

```


Example Playbook
----------------
```yaml
- hosts: proxy
  become: yes
  vars:

  tasks:

  roles:
    - role: haproxy
      haproxy_groups:
        - esb
        - dbproc
```
License
-------

BSD

Author Information
------------------

evgeny.zemlyachenko@stoloto.ru
