gluster
=========

Установка glusterfs клиента. Автоматически монтирует glfs группу.
Редактирует fstab.

Requirements
------------

Хосты должны быть зарегистрированы на satellite.

используемые подписки:

- `ORGLOT_OFFICE_centos7_gluster38_centos7_gluster38_repo`

Role Variables
--------------
- `glusterfs_brick_path: /data/glusterfs`
- `glusterfs_volume_name: vol`

Dependencies
------------

Example inventory
-----------------
```text
[esb]
board-esb1.tsed.orglot.office ansible_host=10.200.81.15
board-esb2.tsed.orglot.office ansible_host=10.200.81.16

```


Example Playbook
----------------
```yaml
- hosts: esb
  become: yes

  roles:
  - role: gluster-client
    tags: gluster-client
```
License
-------

BSD

Author Information
------------------

evgeny.zemlyachenko@stoloto.ru
