galera
=========

Установка и инициализация кластера Galera для rhel7.

Requirements
------------

Хосты должны быть зарегистрированы на satellite.

При установке используются подписки
- `ORGLOT_OFFICE_MariaDB_10_MariaDB_10_1_release_for_RHEL7_-_x86_64`
- `ORGLOT_OFFICE_Percona_Percona_5_release_for_RHEL7`


Role Variables
--------------

`new_cluster: true - запускает galera_new_cluster`

`defaults/main.yml - параметры по умолчанию`

Dependencies
------------

Example inventory
-----------------
```text
[arbitrator]
board-adm.tsed.orglot.office ansible_host=10.200.81.11 cluster_ip=10.200.83.41

[esb]
board-esb1.tsed.orglot.office ansible_host=10.200.81.15 cluster_ip=10.200.83.42
board-esb2.tsed.orglot.office ansible_host=10.200.81.16 cluster_ip=10.200.83.43

[galera:children]
esb
arbitrator
```
`cluster_ip - ip vlan интерфейса, по умолчанию используется ansible_default_ipv4.address`

`arbitrator обязательно указывать последним`



Example Playbook
----------------
```yaml
- hosts: galera
  become: yes
  gather_facts: yes
  vars:
    - hosts: galera
  roles:
    - role: galera
      tags: galera

    - role: galera
      new_cluster: 1
      tags:
        - galera_new
```
License
-------

BSD

Author Information
------------------

evgeny.zemlyachenko@stoloto.ru
