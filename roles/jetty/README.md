jetty
=========

Установка jetty.

Requirements
------------

Хосты должны быть зарегистрированы на satellite.

используемые подписки:

- `ORGLOT_OFFICE_centos7_gluster38_centos7_gluster38_repo`

Role Variables
--------------
- `install: 1` - разрешение на скачивание дистрибутива. 0 - только конфиругация
- `reinstall: 0` - разрешение на удаление всех каталогов jetty для полной переустановки.
- `jetty_uid: 801` - фиксированный uid для статики на glusterfs
- `jetty_home: /opt/jetty`
- `jetty_base: /home/jetty`
- `defaults/main.yml`


Dependencies
------------

```yaml
пакеты:
  - java-1.8.0-oracle (устанавливается автоматически. см. meta/main.yml)
```

Example inventory
-----------------
```text
[esb]
board-esb1.tsed.orglot.office ansible_host=10.200.81.15
board-esb2.tsed.orglot.office ansible_host=10.200.81.16

```


Example Playbook
----------------
```yaml
- hosts: esb
  become: yes

  roles:
  - role: jetty
    tags: jetty
```
License
-------

BSD

Author Information
------------------

evgeny.zemlyachenko@stoloto.ru
