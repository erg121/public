#!/bin/bash

if [ $# -ne 1 ]
then
        echo "Usage: $0 hostname"
        exit 1
fi
HOST=${1}
ADDR=$(host ${HOST} | awk '{print $NF}')
wget --no-check-certificate -qO - https://${ADDR}/check | grep -q "rd-ext-s3-lcp"
if [[ "$?" -ne "0" ]]; then
	exit 1
fi
exit 0
