#/bin/bash

docker stop my-cent
docker rm my-cent

docker rmi --force orglot-httpd
docker rmi --force test-kube-proxy.oets.orglot.office:5000/orglot-httpd:latest

docker build --rm --no-cache -t orglot-httpd:latest .

#docker run -d --name my-cent orglot-httpd:latest

