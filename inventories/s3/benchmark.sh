#!/bin/bash

URL=https://test-kube-lb.oets.orglot.office:30443/check
ulimit -n $((8192*128))
ABDIR=$(mktemp -d)
cd ${ABDIR}

for i in 2 20 200 2000 20000
do
	ab -c $i -t 600 ${URL} | tee ab-$i.log
	sleep 30
done
